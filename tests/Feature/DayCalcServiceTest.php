<?php

namespace Tests\Feature;

use App\Library\Services\DayCalcService;
use Tests\TestCase;
use Illuminate\Support\Facades\Config;

class DayCalcServiceTest extends TestCase
{
    /**
     * @var DayCalcService $dayCalcService
     */
    protected $dayCalcService;

    public function setUp(): void {

        $this->dayCalcService = new DayCalcService();
        parent::setUp();
    }

    /**
     * Test day invalid date.
     *
     * @return void
     * @throws \Exception
     */
    public function test_invalid_date()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('The date is not a valid format: qq/11/2001');
        $date = 'qq/11/2001';
        $this->dayCalcService->validateDate($date);
    }

    /**
     * Test day calc success.
     *
     * @return void
     * @throws \Exception
     */
    public function test_valid_date()
    {
        $date = '1901-01-02';
        $result = $this->dayCalcService->validateDate($date);
        $this->assertTrue($result);
    }

    /**
     * Test missing config.
     *
     * @return void
     * @throws \Exception
     */
    public function test_missing_config()
    {
        $date = '1901-01-02';
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('config not set');
        Config::set('services.dayCalc', null);
        $this->dayCalcService->validateDate($date);
    }

    /**
     * Test missing config (start date).
     *
     * @return void
     * @throws \Exception
     */
    public function test_missing_config_start_date()
    {
        $date = '1901-01-02';
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('end date not configured');
        Config::set('services.dayCalc', ['dateBegin' => '01/01/1901']);
        $this->dayCalcService->validateDate($date);
    }

    /**
     * Test missing config (start date).
     *
     * @return void
     * @throws \Exception
     */
    public function test_missing_config_end_date()
    {
        $date = '1901-01-02';
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('start date not configured');
        Config::set('services.dayCalc', ['dateEnd' => '01/01/1901']);
        $this->dayCalcService->validateDate($date);
    }

    /**
     * Test date before.
     *
     * @return void
     * @throws \Exception
     */
    public function test_date_before()
    {
        $date = '1900-01-02';
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Date: 1900-01-02 must be between 1901-01-01 and 2000-01-01');
        Config::set('services.dayCalc', ['dateBegin' => '01/01/1901', 'dateEnd' => '01/01/2000']);
        $this->dayCalcService->validateDate($date);
    }

    /**
     * Test date after.
     *
     * @return void
     * @throws \Exception
     */
    public function test_date_after()
    {
        $date = '2001-01-02';
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Date: 2001-01-02 must be between 1901-01-01 and 2000-01-01');
        Config::set('services.dayCalc', ['dateBegin' => '01/01/1901', 'dateEnd' => '01/01/2000']);
        $this->dayCalcService->validateDate($date);
    }

    public function test_calculate_days()
    {
        $result = $this->dayCalcService->calculateDays('02/06/1983', '22/06/1983');
        $this->assertSame($result, 19);
        $result = $this->dayCalcService->calculateDays('04/07/1984', '25/12/1984');
        $this->assertSame($result, 173);
        $result = $this->dayCalcService->calculateDays('03/01/1989', '03/08/1983');
        $this->assertSame($result, 1979);
    }

    public function test_date_difference()
    {
        $result = $this->dayCalcService->dateDifference('1983-06-02', '1983-06-22');
        $this->assertSame($result, 19);
        $result = $this->dayCalcService->dateDifference('1984-07-04', '1984-12-25');
        $this->assertSame($result, 173);
        $result = $this->dayCalcService->dateDifference('1989-01-03', '1983-08-03');
        $this->assertSame($result, 1979);
    }
}
