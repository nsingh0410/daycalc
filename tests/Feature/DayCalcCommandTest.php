<?php

namespace Tests\Feature;

use Symfony\Component\Console\Exception\RuntimeException;
use Tests\TestCase;

class DayCalcCommandTest extends TestCase
{
    /**
     * Test day calc success.
     *
     * @return void
     */
    public function test_day_calc_success()
    {
        $this->artisan('command:dry-calc 01/01/2000 03/01/2000')
             ->expectsOutput('1')
             ->assertSuccessful();
    }

    /**
     * Test day calc missing argument.
     *
     * @return void
     */
    public function test_day_calc_file()
    {
        $this->expectException(RuntimeException::class);
        $this->artisan('command:dry-calc 01/01/2000');
    }
}
