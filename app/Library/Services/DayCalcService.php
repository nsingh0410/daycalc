<?php


namespace App\Library\Services;

use DateTime;

/**
 * Class DayCalcService
 *
 * Calculate the total full days.
 * @package App\Library\Services
 */
class DayCalcService
{
    /**
     * Calculates the day difference based on the string.
     *
     * @param string $strStartDate - Starting day as string.
     * @param string $strEndDate - Ending day as string.
     *
     * @return string
     * @throws \Exception
     */
    public function calculateDays(string $strStartDate, string $strEndDate)
    {
        // Format the dates for validation
        $startDate = DateTime::createFromFormat('d/m/Y', $strStartDate)->format('Y-m-d');
        $endDate = DateTime::createFromFormat('d/m/Y', $strEndDate)->format('Y-m-d');

        $this->validateDate($startDate);
        $this->validateDate($endDate);

        return $this->dateDifference($startDate, $endDate);
    }

    /**
     * Validate date based on format.
     *
     * @param string $date
     *
     * @return bool
     * @throws \Exception
     */
    public function validateDate(string $date) :bool
    {
        if (!strtotime($date)) {
           throw new \InvalidArgumentException('The date is not a valid format: ' . $date);
        }
        $config = config('services.dayCalc');
        if (empty($config)) {
            throw New \Exception('config not set');
        }

        if (empty($config['dateBegin'])) {
           throw New \Exception('start date not configured');
        }

        if (empty($config['dateEnd'])) {
            throw New \Exception('end date not configured');
        }

        // convert date tp same format.
        $dateBegin = DateTime::createFromFormat('d/m/Y', $config['dateBegin'])->format('Y-m-d');
        $dateEnd = DateTime::createFromFormat('d/m/Y', $config['dateEnd'])->format('Y-m-d');


        // date range check.
        if ($date < $dateBegin || $date > $dateEnd) {
            throw New \Exception('Date: ' . $date . ' must be between ' . $dateBegin . ' and ' . $dateEnd);
        }

        return true;
    }

    /**
     * Calculate date difference.
     *
     * @param $strStartDate
     * @param $strEndDate
     *
     * @return \DateInterval|false|int
     */
    public function dateDifference(string $strStartDate ,string $strEndDate)
    {
        $startDate = date_create($strStartDate);
        $endDate = date_create($strEndDate);

        $dateDifference = date_diff($startDate, $endDate);

        // Remove partial day and returns result.
        return $dateDifference->days - 1;
    }
}
