<?php

namespace App\Console\Commands;

use App\Library\Services\DayCalcService;
use Illuminate\Console\Command;

class DayCalcCommand extends Command
{
    /**
     * The name and signature of the console command.
     * Start and end date required.
     *
     * @var string
     */
    protected $signature = 'command:dry-calc
                            {start-date}
                            {end-date}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate the full days between a specified date range';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * The first parameter is the start date and the last is the end date.
     *
     * e.g
     * php artisan command:dry-calc 03/01/1989 03/08/1983
     *
     * @param DayCalcService $dayCalcService
     *
     * @return int
     */
    public function handle(DayCalcService $dayCalcService)
    {
        // Get the user input dates.
        $startDate = $this->argument('start-date');
        $endDate = $this->argument('end-date');

        $days = $dayCalcService->calculateDays($startDate, $endDate) . PHP_EOL;

        $this->info($days);

        return Command::SUCCESS;
    }
}
