<?php

namespace App\Providers;

use App\Library\Services\DayCalcService;
use Illuminate\Support\ServiceProvider;

class DayCalcProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
