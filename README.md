# Day Calculator

### Requirements:
php => 8.0 

### Scenario:

You have joined a science project as the latest team member. Scientists on the project are running a
series of experiments and need to calculate the number of full days elapsed in between the
experiment’s start and end dates, i.e. the first and the last day are considered partial days and never
counted. Following this logic, an experiment that has run from 07/11/1972 and 08/11/1972 should
return 0, because there are no fully elapsed days contained in between those dates, and 01/01/2000 to
03/01/2000 should return 1. The solution needs to cater for all valid dates between 01/01/1901 and
31/12/2999.

As the new person on the team, you are tasked with the job. The scientists are using an older,
command-line based system and need at least one way of providing input and output on the terminal
they’re asking you to build the system accordingly. They also don’t just take your word for it when it
comes to saying “I’m done”, they want proof. It turns out they have designed the job as an experiment
and given you a few test cases to pass and validate the output of your program.

### Instructions:

Git clone the repository 

`git clone https://nsingh0410@bitbucket.org/nsingh0410/daycalc.git`

Composer install the libraries

`composer install`

Run the shell script, the first parameter is the first and second parameters is the date range. 

`php artisan command:dry-calc 03/01/1989 03/08/1983`

### Tests

You can run all the test suites by executing the following command in the root directory.

`./vendor/bin/phpunit tests/`

Enjoy 😄



